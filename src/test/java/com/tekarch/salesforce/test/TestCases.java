package com.tekarch.salesforce.test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.tekarch.salesforce.base.Base;
import com.tekarch.salesforce.page.HomePage;
import com.tekarch.salesforce.page.LoginPage;

public class TestCases extends Base {

	LoginPage loginPage;
	HomePage homePage;
	Properties props;

	@BeforeClass
	public void setUp() {
		props = readConfig();
		initializeDriver();
		initializeReport();
		loginPage = new LoginPage();
		homePage = new HomePage();

	}

	@AfterClass
	public void cleanup() {
		cleanUpResources();
	}

	// Log in To SalesForce
	@Test(priority = 3)
	public void testLogin() throws Exception {
		logger = reports.startTest("Login");
		logger.log(LogStatus.INFO, " Test case Started");
		String[][] data;
		try {
			data = readXlData("/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Input/TC1_Login.xls", "Sheet1");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error reading excel file");
			return;
		}

		String actualUser = loginPage.testLogin(data[1][1], data[2][1]);
		String expectedUser = "Keerthi Kethineni";

		logger.log(LogStatus.INFO,
				"Login successful and see the attached screenshot. \r\n" + logger.addScreenCapture(takeScreenshot()));

		Assert.assertEquals(actualUser, expectedUser);
		reports.endTest(logger);
	}

	// User Menu Drop Down
	@Test(priority = 4)
	public void userMenu() throws Exception {
		logger = reports.startTest("User Menu Drop Down");
		logger.log(LogStatus.INFO, " Test case Started");

		boolean userMenuDisplayed = homePage.userMenu();

		logger.log(LogStatus.INFO, "User Menu Drop Down Button clicked successfully");

		Assert.assertTrue(userMenuDisplayed);
		logger.log(LogStatus.INFO, "Drop down is displayed and see the attached screenshot. \r\n"
				+ logger.addScreenCapture(takeScreenshot()));

		homePage.logOut();

		reports.endTest(logger);
	}

	// Password Error Message
	@Test(priority = 1)
	public void passwordErrorMessage() throws Exception {
		logger = reports.startTest("Password Error Message");
		logger.log(LogStatus.INFO, " Test case Started");
		String[][] data;
		try {
			data = readXlData("/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Input/TC1_Login.xls", "Sheet2");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error reading excel file");
			return;
		}
		driver.get(props.getProperty("url"));
		String actualError = loginPage.testLoginFailure(data[1][1], data[2][1]);
		logger.log(LogStatus.INFO, "Username is entered successfully");
		logger.log(LogStatus.INFO, "Password left blank");
		String expectedError = "Please enter your password.";

		Assert.assertEquals(actualError, expectedError);

		logger.log(LogStatus.INFO, "Login Button Clicked successfully");

		logger.log(LogStatus.INFO,
				"Login failure and see the attached screenshot. \r\n" + logger.addScreenCapture(takeScreenshot()));

		reports.endTest(logger);
	}
	// Check Remember Me

	@Test(priority = 2)
	public void checkRememberMe() throws Exception {
		logger = reports.startTest("Check Remember Me");
		logger.log(LogStatus.INFO, " Test case Started");
		String[][] data;
		try {
			data = readXlData("/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Input/TC1_Login.xls", "Sheet1");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error reading excel file");
			return;
		}
		// driver.get(data[0][1]);
		loginPage.loginWithRememberMe(data[1][1], data[2][1]);
		logger.log(LogStatus.INFO, "Login Button Clicked successfully");

		homePage.userMenu();
		logger.log(LogStatus.INFO, "Login sucessful with check remember me");

		homePage.logOut();
		logger.log(LogStatus.INFO, "Logout Button Clicked successfully");

		logger.log(LogStatus.INFO, "Checked Remember Me. \r\n" + logger.addScreenCapture(takeScreenshot()));
		String userName = loginPage.remeberMeLogout();

		Assert.assertEquals(userName, data[1][1]);

		reports.endTest(logger);
	}

	// Forgot Password
	@Test(priority = 5)
	public void forgotPassword() throws Exception {
		logger = reports.startTest("Forgot Password");
		logger.log(LogStatus.INFO, " Test case Started");
		String[][] data;
		try {
			data = readXlData("/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Input/TC1_Login.xls", "Sheet3");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error reading excel file");
			return;
		}
		// driver.get(data[0][1]);

		boolean checkMailDisplayed = loginPage.forgotPswd(data[1][1]);
		logger.log(LogStatus.INFO, "Password Reset Link Clicked successfully");

		logger.log(LogStatus.INFO, "Username is entered successfully");

		logger.log(LogStatus.INFO, "Continue Button Clicked successfully");

		Assert.assertTrue(checkMailDisplayed);

		logger.log(LogStatus.INFO, "Password reset link has been sent to mail and see the attached screenshot. \r\n"
				+ logger.addScreenCapture(takeScreenshot()));

		reports.endTest(logger);
	}

	private Properties readConfig() {
		Properties prop = new Properties();
		try {
			Path resourceDirectory = Paths.get("src", "test", "resources", "config.properties");
			String absolutePath = resourceDirectory.toFile().getAbsolutePath().trim();
			FileInputStream input = new FileInputStream(
					"/Users/sreenivaskuppuru/Documents/Keerthi/QA/selenium-projects/salesforcepom/src/test/resources/cofig.properties");
			prop.load(input);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return prop;
	}
}
