package com.tekarch.salesforce.base;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class Base {
	protected static WebDriver driver;
	protected HSSFWorkbook workbook;
	static protected ExtentReports reports;
	protected ExtentTest logger;

	static final String TEST_REPORTS = "/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Reports/";

	static String pattern = "MM/dd/yyyy";
	static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	public static void initializeDriver() {
		System.setProperty("webdriver.chrome.driver", "/Users/sreenivaskuppuru/Selenium/chromedriver3");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
	}

	public static void cleanUpResources() {
		reports.flush();
		driver.close();
	}
	
	
	public void setInput(WebElement inputElement, String inputField, String inputValue) {
		if (inputElement.isDisplayed()) {
			inputElement.sendKeys(inputValue);
			System.out.println(inputValue + "entered for the " + inputField);
		} else {
			System.out.println(inputElement + "is not displayed.");
		}
	}
	
	
	public void selectField(Select selectElement, String selectValue) {
		selectElement.selectByVisibleText(selectValue);
		System.out.println(selectValue + "is selected.");
	}

	public void click(WebElement buttonElement, String buttonName) {
		if (buttonElement.isDisplayed()) {
			buttonElement.click();
			System.out.println(buttonElement + "is clicked.");
		} else {
			System.out.println(buttonName + "is not displayed");
		}
	}

	public void clickCheckbox(WebElement checkboxElement, String checkBoxName) {
		if (checkboxElement.isDisplayed()) {
			checkboxElement.click();
			System.out.println(checkBoxName + " clicked.");
		} else {
			System.out.println(checkBoxName + " is not displayed");
		}
	}

	public String[][] readXlData(String path, String sheetName) throws Exception {
		FileInputStream fs = new FileInputStream(new File(path));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheet(sheetName);
		int rowCount = sheet.getLastRowNum() + 1;
		int colCount = sheet.getRow(0).getLastCellNum();
		String[][] data = new String[rowCount][colCount];
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				CellType cellType = sheet.getRow(i).getCell(j).getCellType();
				if (cellType == CellType.NUMERIC) {
					if (HSSFDateUtil.isCellDateFormatted(sheet.getRow(i).getCell(j))) {
						data[i][j] = simpleDateFormat.format(sheet.getRow(i).getCell(j).getDateCellValue());
						continue;
					}
					int val = (int) sheet.getRow(i).getCell(j).getNumericCellValue();
					data[i][j] = String.valueOf(val);
				} else
					data[i][j] = sheet.getRow(i).getCell(j).getStringCellValue();
			}
		}
		return (data);
	}

	public static void initializeReport() {
		reports = new ExtentReports(
				TEST_REPORTS + new SimpleDateFormat("'SalesForceReport_'YYYYMMddHHmmss'.html'").format(new Date()));
	}

	public String takeScreenshot() throws IOException {
		TakesScreenshot srcShot = ((TakesScreenshot) driver);
		File srcFile = srcShot.getScreenshotAs(OutputType.FILE);
		String imagePath = TEST_REPORTS + "ScreenShots/"
				+ new SimpleDateFormat("'Image_'YYYYMMddHHmmss'.png'").format(new Date());
		File destFile = new File(imagePath);
		FileUtils.copyFile(srcFile, destFile);
		return imagePath;
	}

	public String takePopupScreenshot() throws HeadlessException, AWTException, IOException {
		BufferedImage image = new Robot()
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		String imagePath = TEST_REPORTS + "ScreenShots/"
				+ new SimpleDateFormat("'Image_'YYYYMMddHHmmss'.png'").format(new Date());
		ImageIO.write(image, "png", new File(imagePath));

		return imagePath;
	}
}

