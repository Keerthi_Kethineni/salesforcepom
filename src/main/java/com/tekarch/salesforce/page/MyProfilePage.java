package com.tekarch.salesforce.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tekarch.salesforce.base.Base;

public class MyProfilePage extends Base {

	@FindBy(id = "My Profile")
	WebElement myProfile;

	public MyProfilePage() {
		PageFactory.initElements(driver, this);
	}
}
