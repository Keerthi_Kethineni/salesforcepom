package com.tekarch.salesforce.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tekarch.salesforce.base.Base;



public class HomePage extends Base {

	@FindBy(id = "userNavButton")
	WebElement uMenuDropDown;

	@FindBy(id = "userNavMenu")
	WebElement userNavMenu;

	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	WebElement SignoutButton;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public boolean userMenu() throws Exception {
		uMenuDropDown.click();
		Thread.sleep(1000);

		return userNavMenu.isDisplayed();
	}

	public void logOut() throws InterruptedException {
		SignoutButton.click();
		Thread.sleep(2000);
	}
}
