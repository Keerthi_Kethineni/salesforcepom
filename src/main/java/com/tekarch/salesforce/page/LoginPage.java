package com.tekarch.salesforce.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tekarch.salesforce.base.Base;

public class LoginPage extends Base {

	@FindBy(id = "username")
	WebElement userName;

	@FindBy(id = "password")
	WebElement password;

	@FindBy(id = "Login")
	WebElement loginButton;

	@FindBy(id = "rememberUn")
	WebElement rememberMe;

	@FindBy(id = "forgot_password_link")
	WebElement forgotPasswordLink;

	@FindBy(id = "un")
	WebElement userName1;

	@FindBy(id = "continue")
	WebElement continueButton;

	@FindBy(id = "userNavLabel")
	WebElement userNavLabel;

	@FindBy(id = "error")
	WebElement error;

	@FindBy(id = "idcard-identity")
	WebElement idcardIdentity;

	@FindBy(id = "clear_link")
	WebElement clearUserName;

	@FindBy(id = "main")
	WebElement checkMailMessage;

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public String testLogin(String usrName, String passwd) throws Exception {
		login(usrName, passwd);

		return userNavLabel.getText();
	}

	public String testLoginFailure(String usrName, String passwd) throws Exception {
		login(usrName, passwd);
		userName.clear();
		password.clear();

		return error.getText();
	}

	public void loginWithRememberMe(String usrName, String passwd) throws InterruptedException {
		loginWithRemember(usrName, passwd);

	}

	public String remeberMeLogout() throws Exception {
		String user = idcardIdentity.getText();
		clearUserName.click();
		Thread.sleep(500);

		return user;
	}

	public boolean forgotPswd(String usrName1) throws InterruptedException {
		forgotPasswordLink.click();
		userName1.sendKeys(usrName1);
		continueButton.click();
		Thread.sleep(500);
		return checkMailMessage.isDisplayed();

	}

	private void login(String usrName, String passwd) throws InterruptedException {
		userName.sendKeys(usrName);
		Thread.sleep(500);
		password.sendKeys(passwd);
		Thread.sleep(500);
		loginButton.click();
		Thread.sleep(1000);
	}

	private void loginWithRemember(String usrName, String passwd) throws InterruptedException {
		userName.sendKeys(usrName);
		Thread.sleep(500);
		password.sendKeys(passwd);
		Thread.sleep(500);
		rememberMe.click();
		Thread.sleep(500);
		loginButton.click();
		Thread.sleep(1000);
	}
}
